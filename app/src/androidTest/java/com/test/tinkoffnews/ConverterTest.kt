package com.test.tinkoffnews

import android.support.test.runner.AndroidJUnit4
import com.test.tinkoffnews.storage.Converters
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.threeten.bp.LocalDateTime

@RunWith(AndroidJUnit4::class)
class ConverterTest {
    private val converter = Converters()

    @Test
    fun testLocalDateTimeConverter() {
        val now = LocalDateTime.now()
        val milli = converter.dateToEpochMilli(now)
        val converted = converter.fromEpochMilli(milli)
        assertEquals(now, converted)
    }
}
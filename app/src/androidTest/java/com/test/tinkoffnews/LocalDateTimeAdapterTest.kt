package com.test.tinkoffnews

import android.support.test.runner.AndroidJUnit4
import com.google.gson.GsonBuilder
import com.test.tinkoffnews.api.LocalDateTimeAdapter
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.threeten.bp.LocalDateTime

@RunWith(AndroidJUnit4::class)
class LocalDateTimeAdapterTest {
    private val gson = GsonBuilder()
            .registerTypeAdapter(LocalDateTime::class.java, LocalDateTimeAdapter())
            .create()

    @Test
    fun testLocalDateTimeAdapter() {
        val now = LocalDateTime.now()
        val json = gson.toJson(now)
        val converted = gson.fromJson<LocalDateTime>(json, LocalDateTime::class.java)
        assertEquals(now, converted)
    }
}
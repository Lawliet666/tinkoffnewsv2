package com.test.tinkoffnews

import android.content.Context
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId

fun Long.toLocalDateTime(): LocalDateTime = Instant.ofEpochMilli(this)
        .atZone(ZoneId.systemDefault())
        .toLocalDateTime()

fun LocalDateTime.toEpochMilli(): Long = this.atZone(ZoneId.systemDefault())
        .toInstant()
        .toEpochMilli()


fun Int.resourceName(context: Context = App.app): String = context.resources.getResourceName(this)
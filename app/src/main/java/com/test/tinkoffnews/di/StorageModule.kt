package com.test.tinkoffnews.di

import android.content.Context
import com.test.tinkoffnews.api.ApiService
import com.test.tinkoffnews.storage.AppDatabase
import com.test.tinkoffnews.storage.Repository
import com.test.tinkoffnews.storage.RepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {

    @Singleton
    @Provides
    fun provideDatabase(context: Context): AppDatabase {
        return AppDatabase.create(context)
    }

    @Singleton
    @Provides
    fun provideRepository(appDatabase: AppDatabase, apiService: ApiService): Repository {
        return RepositoryImpl(appDatabase, apiService)
    }
}
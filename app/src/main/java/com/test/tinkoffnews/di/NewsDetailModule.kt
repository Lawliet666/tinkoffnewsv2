package com.test.tinkoffnews.di

import com.test.tinkoffnews.api.ErrorMessageHandler
import com.test.tinkoffnews.storage.Repository
import com.test.tinkoffnews.ui.news.detail.NewsDetailPresenter
import dagger.Module
import dagger.Provides

@Module
class NewsDetailModule {

    @Provides
    fun providePresenter(repository: Repository, errorMessageHandler: ErrorMessageHandler): NewsDetailPresenter {
        return NewsDetailPresenter(repository, errorMessageHandler)
    }
}
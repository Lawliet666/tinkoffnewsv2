package com.test.tinkoffnews.di

import com.test.tinkoffnews.ui.news.NewsListFragment
import com.test.tinkoffnews.ui.news.detail.NewsDetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector
import javax.inject.Scope

@Module
abstract class FragmentBinding {

    @PerFragment
    @ContributesAndroidInjector(modules = [NewsListModule::class])
    abstract fun provideNewsListFragment(): NewsListFragment

    @PerFragment
    @ContributesAndroidInjector(modules = [NewsDetailModule::class])
    abstract fun provideNewsDetailFragment(): NewsDetailFragment


    @Scope
    @Retention(AnnotationRetention.RUNTIME)
    annotation class PerFragment
}
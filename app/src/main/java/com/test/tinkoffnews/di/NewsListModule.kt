package com.test.tinkoffnews.di

import com.test.tinkoffnews.api.ErrorMessageHandler
import com.test.tinkoffnews.storage.Repository
import com.test.tinkoffnews.ui.news.NewsListPresenter
import dagger.Module
import dagger.Provides

@Module
class NewsListModule {

    @Provides
    fun providePresenter(repository: Repository, errorMessageHandler: ErrorMessageHandler): NewsListPresenter {
        return NewsListPresenter(repository, errorMessageHandler)
    }
}
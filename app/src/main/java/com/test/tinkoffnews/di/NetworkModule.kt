package com.test.tinkoffnews.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.test.tinkoffnews.BuildConfig
import com.test.tinkoffnews.api.ApiService
import com.test.tinkoffnews.api.ErrorMessageHandler
import com.test.tinkoffnews.api.LocalDateTimeAdapter
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import org.threeten.bp.LocalDateTime
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {
    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder().build()
    }

    @Singleton
    @Provides
    fun provideGson(): Gson {
        return GsonBuilder()
                .registerTypeAdapter(LocalDateTime::class.java, LocalDateTimeAdapter())
                .create()
    }

    @Singleton
    @Provides
    fun provideApiService(client: OkHttpClient, gson: Gson): ApiService {
        val retrofit = Retrofit.Builder()
                .client(client)
                .baseUrl(BuildConfig.HOST)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .build()
        return retrofit.create(ApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideErrorHandler(context: Context) = ErrorMessageHandler(context)
}
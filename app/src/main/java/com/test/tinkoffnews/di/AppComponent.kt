package com.test.tinkoffnews.di

import com.test.tinkoffnews.App
import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, StorageModule::class, NetworkModule::class,
    FragmentBinding ::class])
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()
}
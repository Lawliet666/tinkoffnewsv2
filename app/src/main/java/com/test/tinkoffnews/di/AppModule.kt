package com.test.tinkoffnews.di

import android.app.Application
import android.content.Context
import com.test.tinkoffnews.App
import dagger.Binds
import dagger.Module
import dagger.android.support.AndroidSupportInjectionModule

@Module(includes = [AndroidSupportInjectionModule::class])
abstract class AppModule {
    @Binds
    abstract fun application(app: App): Application

    @Binds
    abstract fun context(app: App): Context
}
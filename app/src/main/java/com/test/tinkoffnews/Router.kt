package com.test.tinkoffnews

interface Router {
    fun navigateToNewsList()
    fun navigateToNewsDetail(id: String)
}
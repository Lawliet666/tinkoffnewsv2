package com.test.tinkoffnews.api

import com.test.tinkoffnews.api.models.NewsContentModel
import com.test.tinkoffnews.api.models.NewsPreviewModel
import com.test.tinkoffnews.api.models.ResponseModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("news")
    fun getNews(): Single<ResponseModel<List<NewsPreviewModel>>>

    @GET("news_content")
    fun getNewsContent(@Query("id") id: String): Single<ResponseModel<NewsContentModel>>
}
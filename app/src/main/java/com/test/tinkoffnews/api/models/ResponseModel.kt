package com.test.tinkoffnews.api.models

class ResponseModel<T>(val payload: T,
                       val resultCode: String,
                       val trackingId: String) {
    fun isSuccess() = resultCode == RESULT_OK

    companion object {
        const val RESULT_OK = "OK"
    }
}
package com.test.tinkoffnews.api.models

import org.threeten.bp.LocalDateTime

data class NewsPreviewModel(val id: String,
                            val name: String,
                            var text: String,
                            val publicationDate: LocalDateTime,
                            val bankInfoTypeId: Int)
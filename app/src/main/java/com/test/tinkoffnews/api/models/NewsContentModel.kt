package com.test.tinkoffnews.api.models

import org.threeten.bp.LocalDateTime

data class NewsContentModel(val title: NewsPreviewModel,
                            val creationDate: LocalDateTime,
                            val lastModificationDate: LocalDateTime,
                            val content: String,
                            val bankInfoTypeId: Int,
                            val typeId: String)
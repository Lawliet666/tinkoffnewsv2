package com.test.tinkoffnews.api

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter
import com.test.tinkoffnews.toEpochMilli
import com.test.tinkoffnews.toLocalDateTime
import org.threeten.bp.LocalDateTime

class LocalDateTimeAdapter : TypeAdapter<LocalDateTime>() {

    override fun write(output: JsonWriter, value: LocalDateTime?) {
        val milliseconds = value?.toEpochMilli()
        val jsonWriter = output.beginObject()
                .name("milliseconds")
        if (milliseconds == null)
            jsonWriter.nullValue()
        else
            jsonWriter.value(milliseconds)

        jsonWriter.endObject()
    }

    override fun read(input: JsonReader): LocalDateTime {
        input.beginObject()
        input.nextName()
        val milliseconds = input.nextLong()
        input.endObject()
        return milliseconds.toLocalDateTime()
    }
}
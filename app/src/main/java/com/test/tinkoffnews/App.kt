package com.test.tinkoffnews

import android.app.Activity
import android.app.Application
import android.support.v4.app.Fragment
import com.jakewharton.threetenabp.AndroidThreeTen
import com.test.tinkoffnews.di.DaggerAppComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


class App : Application(), HasActivityInjector, HasSupportFragmentInjector {

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun activityInjector() = activityDispatchingAndroidInjector
    override fun supportFragmentInjector() = fragmentDispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()
        app = this
        DaggerAppComponent.builder()
                .create(this)
                .inject(this)
        AndroidThreeTen.init(this)
    }

    companion object {
        /**
         *  Application haven't Content Provider and this used just for kotlin extension function
         */
        lateinit var app: App
    }
}
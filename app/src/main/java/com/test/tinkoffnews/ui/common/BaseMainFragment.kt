package com.test.tinkoffnews.ui.common

import android.content.Context
import android.support.annotation.StringRes
import android.support.v7.widget.Toolbar
import com.arellomobile.mvp.MvpAppCompatFragment
import com.test.tinkoffnews.MainActivity
import com.test.tinkoffnews.Router
import dagger.android.support.AndroidSupportInjection

abstract class BaseMainFragment : MvpAppCompatFragment() {
    protected val router: Router
        get() = context as Router

    private val toolbar: Toolbar
        get() = (activity as MainActivity).getToolbar()

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        if (context !is Router) {
            throw ClassCastException("Parent activity must implement Router")
        }
    }

    protected fun setTitle(title: CharSequence) {
        toolbar.title = title
    }

    protected fun setTitle(@StringRes resId: Int) {
        toolbar.setTitle(resId)
    }
}
package com.test.tinkoffnews.ui.news

import com.arellomobile.mvp.InjectViewState
import com.test.tinkoffnews.api.ErrorMessageHandler
import com.test.tinkoffnews.ui.common.BaseMvpPresenter
import com.test.tinkoffnews.storage.Repository
import io.reactivex.android.schedulers.AndroidSchedulers

@InjectViewState
class NewsListPresenter(private val repository: Repository,
                        private val errorMessageHandler: ErrorMessageHandler) : BaseMvpPresenter<NewsListView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        repository.getNews()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showLoadingState(visible = true) }
                .doOnEach {
                    if (it.isOnError || it.isOnComplete || it.isOnNext)
                        viewState.showLoadingState(visible = false)
                }
                .subscribe({ viewState.showNews(it) }, { handleError(it) })
                .untilDestroy()
    }

    fun onRefresh() {
        repository.syncNews()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { viewState.showLoadingState(visible = true) }
                .doOnTerminate { viewState.showLoadingState(visible = false) }
                .subscribe({}, { handleError(it) })
                .untilDestroy()
    }

    private fun handleError(it: Throwable) {
        it.printStackTrace()
        val message = errorMessageHandler.getMessage(it)
        viewState.showError(message)
    }
}
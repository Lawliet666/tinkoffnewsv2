package com.test.tinkoffnews.ui.common.recyclerview

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.util.SparseArray
import android.view.ViewGroup
import com.test.tinkoffnews.resourceName

open class RendererListAdapter<T : ItemModel, H : BaseViewHolder>(diffCallback: DiffUtil.ItemCallback<T> = BaseDiffCallback()) :
        ListAdapter<T, H>(diffCallback), ModelProvider<T> {
    private val renderers = SparseArray<TypeRenderer<T, H>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): H {
        val renderer = renderers.get(viewType)
        if (renderer != null) {
            return renderer.create(parent)
        } else {
            throw RuntimeException("Not supported Item View Type: ${viewType.resourceName()} in " +
                    javaClass.simpleName)
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun registerRenderer(vararg renderers: TypeRenderer<out T, out H>) {
        renderers.forEach { renderer ->
            val type = renderer.type
            if (this.renderers.get(type) == null) {
                this.renderers.put(type, renderer as TypeRenderer<T, H>)
                renderer.attachModelProvider(this)
            } else {
                throw RuntimeException("ViewRenderer with the same type: ${type.resourceName()} already " +
                        "exist in ${javaClass.simpleName}")
            }
        }
    }

    override fun onBindViewHolder(holder: H, position: Int) {
        val item = getItem(position)
        val renderer = renderers.get(item.itemType)
        if (renderer != null) {
            renderer.bind(item, holder)
            onBindViewHolderWithItem(holder, position, item)
        } else {
            throw RuntimeException("Not supported Item View Type: ${item.itemType.resourceName()} in " +
                    javaClass.simpleName)
        }
    }

    open fun onBindViewHolderWithItem(holder: H, position: Int, item: T) {

    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        return item.itemType
    }

    override fun onViewRecycled(holder: H) {
        super.onViewRecycled(holder)
        val renderer = renderers.get(holder.itemViewType)
        renderer.clear(holder)
    }

    override fun onViewDetachedFromWindow(holder: H) {
        super.onViewDetachedFromWindow(holder)
        val renderer = renderers.get(holder.itemViewType)
        renderer.detachedFromWindow(holder)
    }


    @Suppress("UNCHECKED_CAST")
    override fun <M : T> getItemModel(position: Int): M {
        return if (position < itemCount)
            getItem(position) as M
        else
            throw NoSuchElementException("item on $position not found")
    }
}
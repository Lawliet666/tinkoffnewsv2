package com.test.tinkoffnews.ui.news

import com.test.tinkoffnews.ui.common.recyclerview.ItemModel

interface SimpleNewsViewModel : ItemModel {
    val title: CharSequence
}
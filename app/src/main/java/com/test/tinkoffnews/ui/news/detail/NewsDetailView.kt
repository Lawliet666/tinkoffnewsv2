package com.test.tinkoffnews.ui.news.detail

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

interface NewsDetailView : MvpView {
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showContent(content: String)

    @StateStrategyType(SingleStateStrategy::class)
    fun showTitle(title: CharSequence)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showError(message: CharSequence)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setLoadingState(visible: Boolean)
}
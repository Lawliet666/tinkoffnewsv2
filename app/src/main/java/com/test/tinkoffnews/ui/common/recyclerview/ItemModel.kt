package com.test.tinkoffnews.ui.common.recyclerview

interface ItemModel {
    val id: String
    val itemType: Int
}
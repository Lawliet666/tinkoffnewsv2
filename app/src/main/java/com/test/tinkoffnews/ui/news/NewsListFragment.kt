package com.test.tinkoffnews.ui.news

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.test.tinkoffnews.R
import com.test.tinkoffnews.ui.common.BaseMainFragment
import com.test.tinkoffnews.ui.common.recyclerview.BaseViewHolder
import com.test.tinkoffnews.ui.common.recyclerview.ItemModel
import com.test.tinkoffnews.ui.common.recyclerview.RendererListAdapter
import kotlinx.android.synthetic.main.main_fragment.*
import javax.inject.Inject

class NewsListFragment : BaseMainFragment(), NewsListView {
    companion object {
        fun newInstance() = NewsListFragment()
    }

    @Inject
    @InjectPresenter
    lateinit var presenter: NewsListPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    private lateinit var adapter: RendererListAdapter<ItemModel, BaseViewHolder>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = RendererListAdapter()
        adapter.registerRenderer(SimpleNewsRenderer(requireContext(), { newsId ->
            router.navigateToNewsDetail(newsId)
        }))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent)
        rvContentList.adapter = adapter
        rvContentList.layoutManager = LinearLayoutManager(context)
        rvContentList.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        swipeRefreshLayout.setOnRefreshListener {
            presenter.onRefresh()
        }
    }

    override fun onResume() {
        super.onResume()
        setTitle(R.string.news_title)
    }

    override fun showNews(items: List<ItemModel>) {
        adapter.submitList(items)
    }

    override fun showError(message: CharSequence) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun showLoadingState(visible: Boolean) {
        swipeRefreshLayout.isRefreshing = visible
    }
}

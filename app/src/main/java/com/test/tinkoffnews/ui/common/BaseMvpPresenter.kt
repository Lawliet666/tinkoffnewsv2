package com.test.tinkoffnews.ui.common

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseMvpPresenter<V : MvpView> : MvpPresenter<V>() {
    private val presenterLifecycleDisposable = CompositeDisposable()

    fun Disposable.untilDestroy() {
        presenterLifecycleDisposable.add(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenterLifecycleDisposable.clear()
    }
}
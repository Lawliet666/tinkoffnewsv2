package com.test.tinkoffnews.ui.common.recyclerview

import android.support.v7.util.DiffUtil

class BaseDiffCallback<T : ItemModel> : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T) = oldItem.id == newItem.id
    override fun areContentsTheSame(oldItem: T, newItem: T) = oldItem == newItem
}
package com.test.tinkoffnews.ui.common.recyclerview

interface ModelProvider<T> {
    fun <M : T> getItemModel(position: Int): M
}
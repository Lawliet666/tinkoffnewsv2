package com.test.tinkoffnews.ui.news.detail

import android.os.Handler
import com.arellomobile.mvp.InjectViewState
import com.test.tinkoffnews.api.ErrorMessageHandler
import com.test.tinkoffnews.ui.common.BaseMvpPresenter
import com.test.tinkoffnews.storage.Repository
import io.reactivex.android.schedulers.AndroidSchedulers

@InjectViewState
class NewsDetailPresenter(private val repository: Repository,
                          private val errorMessageHandler: ErrorMessageHandler) : BaseMvpPresenter<NewsDetailView>() {
    lateinit var newsId: String

    private var handler: Handler = Handler()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        repository.getNewsContent(newsId)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { setLoadingState(visible = true) }
                .doAfterTerminate { setLoadingState(visible = false) }
                .subscribe({
                    viewState.showContent(it.content.content)
                    viewState.showTitle(it.preview.text)
                }, { handleError(it) })
                .untilDestroy()
    }

    private fun setLoadingState(visible: Boolean) {
        handler.removeCallbacksAndMessages(null)
        handler.postDelayed({
            viewState.setLoadingState(visible = visible)
        }, 500)
    }

    private fun handleError(e: Throwable) {
        e.printStackTrace()
        val message = errorMessageHandler.getMessage(e)
        viewState.showError(message)
    }
}
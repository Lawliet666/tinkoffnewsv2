package com.test.tinkoffnews.ui.news

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.test.tinkoffnews.ui.common.recyclerview.ItemModel

interface NewsListView : MvpView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showNews(items: List<ItemModel>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showError(message: CharSequence)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showLoadingState(visible: Boolean)
}
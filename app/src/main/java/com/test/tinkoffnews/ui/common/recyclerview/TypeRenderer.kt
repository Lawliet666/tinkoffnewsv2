package com.test.tinkoffnews.ui.common.recyclerview

import android.content.Context
import android.view.ViewGroup
import java.lang.ref.WeakReference

abstract class TypeRenderer<M : ItemModel, H : BaseViewHolder>(val type: Int,
                                                               protected val context: Context) {
    private var adapterReference: WeakReference<ModelProvider<M>>? = null

    protected val adapter: ModelProvider<M>?
        get() = adapterReference?.get()

    fun attachModelProvider(adapter: ModelProvider<M>) {
        adapterReference = WeakReference(adapter)
    }

    fun <T : M> getItem(position: Int): T {
        return adapter?.getItemModel(position)!!
    }

    abstract fun create(parent: ViewGroup): H
    abstract fun bind(model: M, holder: H)

    open fun clear(holder: H) {}
    open fun detachedFromWindow(holder: H) {}
}
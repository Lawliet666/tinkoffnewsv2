package com.test.tinkoffnews.ui.news.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.test.tinkoffnews.Constants.ARG_NEWS_ID
import com.test.tinkoffnews.R
import com.test.tinkoffnews.ui.common.BaseMainFragment
import kotlinx.android.synthetic.main.detail_fragment.*
import javax.inject.Inject


class NewsDetailFragment : BaseMainFragment(), NewsDetailView {

    companion object {
        fun newInstance(id: String): NewsDetailFragment {
            return NewsDetailFragment().apply {
                val args = Bundle()
                args.putString(ARG_NEWS_ID, id)
                arguments = args
            }
        }
    }

    @Inject
    @InjectPresenter
    lateinit var presenter: NewsDetailPresenter

    @ProvidePresenter
    fun providePresenter(): NewsDetailPresenter {
        presenter.newsId = arguments!!.getString(ARG_NEWS_ID)
        return presenter
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.detail_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (savedInstanceState != null) {
            webView.restoreState(savedInstanceState)
        }
        webView.settings.defaultFontSize = 18
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        webView.saveState(outState)
    }

    override fun showContent(content: String) {
        webView.loadData(content, "text/html", "UTF-8")
    }

    override fun showTitle(title: CharSequence) {
        setTitle(title)
    }

    override fun showError(message: CharSequence) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun setLoadingState(visible: Boolean) {
        when {
            visible && viewSwitcher.nextView.id == R.id.progressBar -> viewSwitcher.showNext()
            !visible && viewSwitcher.nextView.id == R.id.webView -> viewSwitcher.showNext()
        }
    }
}
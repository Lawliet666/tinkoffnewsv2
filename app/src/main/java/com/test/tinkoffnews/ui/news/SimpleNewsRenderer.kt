package com.test.tinkoffnews.ui.news

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.test.tinkoffnews.R
import com.test.tinkoffnews.ui.common.recyclerview.BaseViewHolder
import com.test.tinkoffnews.ui.common.recyclerview.TypeRenderer
import com.test.tinkoffnews.ui.news.SimpleNewsRenderer.SimpleNewsViewHolder
import kotlinx.android.synthetic.main.recycler_view_news_item.view.*

class SimpleNewsRenderer(context: Context, val onItemClick: (id: String) -> Unit) :
        TypeRenderer<SimpleNewsViewModel, SimpleNewsViewHolder>(R.id.recycler_type_simple_news, context) {

    override fun create(parent: ViewGroup): SimpleNewsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.recycler_view_news_item, parent, false)
        return SimpleNewsViewHolder(view)
    }

    override fun bind(model: SimpleNewsViewModel, holder: SimpleNewsViewHolder) {
        holder.bind(model)
    }

    inner class SimpleNewsViewHolder(itemView: View) : BaseViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                runIfHasPosition {
                    val item = getItem<SimpleNewsViewModel>(adapterPosition)
                    onItemClick(item.id)
                }
            }
        }

        fun bind(newsPreviewModel: SimpleNewsViewModel) {
            itemView.tvTitle.text = newsPreviewModel.title
        }
    }
}
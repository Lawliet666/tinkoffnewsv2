package com.test.tinkoffnews.storage

import android.os.Build
import android.text.Html
import com.test.tinkoffnews.api.ApiService
import com.test.tinkoffnews.api.models.NewsContentModel
import com.test.tinkoffnews.api.models.NewsPreviewModel
import com.test.tinkoffnews.storage.entities.NewsContentEntity
import com.test.tinkoffnews.storage.entities.NewsPreviewEntity
import com.test.tinkoffnews.storage.entities.PreviewWithContent
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class RepositoryImpl(private val appDatabase: AppDatabase,
                     private val apiService: ApiService) : Repository {

    override fun getNews(): Flowable<List<NewsPreviewEntity>> {
        return appDatabase.newsDao().getNews()
                .doOnSubscribe { startUpdateNews() }
    }

    private fun startUpdateNews() {
        syncNews().onErrorComplete()
                .subscribe()
    }

    override fun getNewsContent(id: String): Single<PreviewWithContent> {
        return appDatabase.newsDao().getNewsWithContent(id)
                .switchIfEmpty(loadContent(id))
                .subscribeOn(Schedulers.io())
    }

    private fun loadContent(id: String): Single<PreviewWithContent> {
        return apiService.getNewsContent(id)
                .doOnSuccess { saveToDatabase(it.payload) }
                .flatMapMaybe { appDatabase.newsDao().getNewsWithContent(id) }
                .toSingle()
    }

    private fun saveToDatabase(model: NewsContentModel) {
        val previewModel = model.title
        replaceHtmlCharacters(previewModel)
        saveToDatabase(previewModel)
        val contentEntity = model.toEntity()
        appDatabase.newsDao().insert(contentEntity)
    }

    private fun saveToDatabase(models: List<NewsPreviewModel>) {
        val previewEntities = models.map { it.toEntity() }
        appDatabase.runInTransaction {
            appDatabase.newsDao().deleteAllPreview()
            appDatabase.newsDao().insert(previewEntities)
        }
    }

    private fun saveToDatabase(model: NewsPreviewModel) {
        val previewEntity = model.toEntity()
        appDatabase.newsDao().insert(previewEntity)
    }

    override fun syncNews(): Completable {
        return apiService.getNews()
                .map {
                    it.payload.forEach { replaceHtmlCharacters(it) }
                    it.payload
                }
                .doOnSuccess { saveToDatabase(it) }
                .ignoreElement()
    }

    private fun replaceHtmlCharacters(model: NewsPreviewModel) {
        model.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(model.text, Html.FROM_HTML_MODE_LEGACY).toString()
        } else {
            @Suppress("DEPRECATION")
            Html.fromHtml(model.text).toString()
        }
    }

    private fun NewsPreviewModel.toEntity() = NewsPreviewEntity(this.id, this.name, this.text,
            this.publicationDate)

    private fun NewsContentModel.toEntity() = NewsContentEntity(this.title.id, this.creationDate,
            this.lastModificationDate, this.content, this.typeId)
}
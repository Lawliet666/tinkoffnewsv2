package com.test.tinkoffnews.storage.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.test.tinkoffnews.R
import com.test.tinkoffnews.storage.entities.NewsPreviewEntity.Companion.TABLE_NAME
import com.test.tinkoffnews.ui.news.SimpleNewsViewModel
import org.threeten.bp.LocalDateTime

@Entity(tableName = TABLE_NAME)
data class NewsPreviewEntity(@PrimaryKey
                             override val id: String,
                             var name: String,
                             var text: String,
                             val publicationDate: LocalDateTime) : SimpleNewsViewModel {

    @Ignore
    override val title: CharSequence = text

    @Ignore
    override val itemType = R.id.recycler_type_simple_news

    companion object {
        const val TABLE_NAME = "news"
    }
}
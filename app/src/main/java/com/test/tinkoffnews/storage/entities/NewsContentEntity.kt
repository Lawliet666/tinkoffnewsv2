package com.test.tinkoffnews.storage.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.test.tinkoffnews.storage.entities.NewsContentEntity.Companion.TABLE_NAME
import org.threeten.bp.LocalDateTime

@Entity(tableName = TABLE_NAME)
data class NewsContentEntity(@PrimaryKey
                             val previewId: String,
                             val creationDate: LocalDateTime,
                             val lastModificationDate: LocalDateTime,
                             val content: String,
                             val typeId: String) {

    companion object {
        const val TABLE_NAME = "news_content"
    }
}
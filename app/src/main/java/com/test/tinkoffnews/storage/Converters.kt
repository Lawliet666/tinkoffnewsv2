package com.test.tinkoffnews.storage

import android.arch.persistence.room.TypeConverter
import com.test.tinkoffnews.toEpochMilli
import com.test.tinkoffnews.toLocalDateTime
import org.threeten.bp.LocalDateTime

class Converters {
    @TypeConverter
    fun fromEpochMilli(value: Long?): LocalDateTime? {
        return value?.toLocalDateTime()
    }

    @TypeConverter
    fun dateToEpochMilli(date: LocalDateTime?): Long? {
        return date?.toEpochMilli()
    }
}
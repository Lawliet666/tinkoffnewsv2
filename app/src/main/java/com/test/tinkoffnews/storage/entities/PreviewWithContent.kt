package com.test.tinkoffnews.storage.entities

import android.arch.persistence.room.Embedded

class PreviewWithContent(@Embedded val preview: NewsPreviewEntity,
                         @Embedded val content: NewsContentEntity)
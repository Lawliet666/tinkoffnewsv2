package com.test.tinkoffnews.storage

import com.test.tinkoffnews.storage.entities.NewsPreviewEntity
import com.test.tinkoffnews.storage.entities.PreviewWithContent
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface Repository {
    fun getNews(): Flowable<List<NewsPreviewEntity>>
    fun getNewsContent(id: String): Single<PreviewWithContent>
    fun syncNews(): Completable
}
package com.test.tinkoffnews.storage

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.test.tinkoffnews.storage.entities.NewsContentEntity
import com.test.tinkoffnews.storage.entities.NewsPreviewEntity
import com.test.tinkoffnews.storage.entities.PreviewWithContent
import io.reactivex.Flowable
import io.reactivex.Maybe

@Dao
interface NewsDao {
    @Query("SELECT * FROM ${NewsPreviewEntity.TABLE_NAME} ORDER BY publicationDate DESC")
    fun getNews(): Flowable<List<NewsPreviewEntity>>

    @Query(SELECT_PREVIEW_WITH_CONTENT)
    fun getNewsWithContent(id: String): Maybe<PreviewWithContent>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(news: List<NewsPreviewEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(news: NewsPreviewEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(newsContent: NewsContentEntity)

    @Query("DELETE FROM ${NewsPreviewEntity.TABLE_NAME}")
    fun deleteAllPreview()

    @Query("DELETE FROM ${NewsContentEntity.TABLE_NAME}")
    fun deleteAllContent()

    companion object {
        const val SELECT_PREVIEW_WITH_CONTENT = "SELECT * FROM ${NewsContentEntity.TABLE_NAME} content " +
                "LEFT JOIN ${NewsPreviewEntity.TABLE_NAME} preview ON content.previewId = preview.id " +
                "WHERE content.previewId = :id"
    }
}
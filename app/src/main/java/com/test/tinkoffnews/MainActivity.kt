package com.test.tinkoffnews

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.FragmentTransaction.TRANSIT_FRAGMENT_FADE
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import com.test.tinkoffnews.ui.news.NewsListFragment
import com.test.tinkoffnews.ui.news.detail.NewsDetailFragment
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : AppCompatActivity(), Router {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            navigateToNewsList()
        }

        supportFragmentManager.addOnBackStackChangedListener {
            checkBackArrowVisibility()
        }
        checkBackArrowVisibility()
    }

    override fun navigateToNewsList() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, NewsListFragment.newInstance())
                .commitNow()
    }

    override fun navigateToNewsDetail(id: String) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, NewsDetailFragment.newInstance(id))
                .setTransition(TRANSIT_FRAGMENT_FADE)
                .addToBackStack(null)
                .commit()
    }

    private fun checkBackArrowVisibility() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            showBackArrow(true)
        } else {
            showBackArrow(false)
        }
    }

    @SuppressLint("PrivateResource")
    private fun showBackArrow(show: Boolean) {
        if (show) {
            toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material)
            toolbar.setNavigationOnClickListener { onBackPressed() }
        } else {
            toolbar.navigationIcon = null
            toolbar.setNavigationOnClickListener(null)
        }
    }

    fun getToolbar(): Toolbar {
        return toolbar
    }
}
